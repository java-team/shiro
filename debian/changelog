shiro (1.3.2-6) unstable; urgency=medium

  * Depend on libservlet-api-java instead of libservlet3.1-java
  * Ignore the deprecated guice-multibindings dependency
  * Standards-Version updated to 4.7.0
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 17 Dec 2024 15:36:48 +0100

shiro (1.3.2-5) unstable; urgency=medium

  * Team upload.
  * Update patch for Spring Framework 4.3.x build failure.
  * Cherry-pick upstream patch with Guice improvements.
  * CVE-2020-1957: Fix a path-traversal issue where a specially-crafted request
    could cause an authentication bypass. (Closes: #955018)
  * CVE-2020-11989: Fix an encoding issue introduced in the handling of the
    previous CVE-2020-1957 path-traversal issue which could have also caused an
    authentication bypass.
  * CVE-2020-13933: Fix an authentication bypass resulting from a specially
    crafted HTTP request. (Closes: #968753)
  * CVE-2020-17510: Fix an authentication bypass resulting from a specially
    crafted HTTP request.

 -- Roberto C. Sánchez <roberto@debian.org>  Fri, 27 Aug 2021 13:10:19 -0400

shiro (1.3.2-4) unstable; urgency=medium

  * Team upload.
  * Remove powermock from B-D. See #875358.
  * Declare compliance with Debian Policy 4.3.0.
  * Install the NOTICE file with libshiro-java.docs.

 -- Markus Koschany <apo@debian.org>  Fri, 01 Mar 2019 22:36:03 +0100

shiro (1.3.2-3) unstable; urgency=medium

  * Fixed the build failure with Java 11 (Closes: #912390)
  * Standards-Version updated to 4.2.1
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 29 Nov 2018 14:37:03 +0100

shiro (1.3.2-2) unstable; urgency=medium

  * Team upload.
  * Add missing build-dep on junit4 (Closes: #871325)

 -- tony mancill <tmancill@debian.org>  Thu, 17 Aug 2017 21:57:24 -0700

shiro (1.3.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - New build dependency on libpowermock-java
    - Ignore the new hazelcast module
  * Depend on libtaglibs-standard-spec-java instead of libjstl1.1-java
  * debian/watch: Track the release tags on GitHub
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 16 Nov 2016 15:30:28 +0100

shiro (1.2.5-2) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Spring Framework 4.3.x (Closes: #834471)
  * Build with the DH sequencer instead of CDBS
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 19 Aug 2016 19:57:14 +0200

shiro (1.2.5-1) unstable; urgency=high

  * Team upload.
  * New upstream release.
    Fixes CVE-2016-4437 (Closes: #826653)
  * Bump Standards-Version to 3.9.8 (no changes).
  * Include reproducible build patch.
    Thank you to Chris Lamb. (Closes: #797296)

 -- tony mancill <tmancill@debian.org>  Sun, 12 Jun 2016 11:57:59 -0700

shiro (1.2.4-1) unstable; urgency=medium

  * New upstream release
    - Removed the dependency on libguava-java
    - Refreshed the patch
    - Ignore the maven-toolchains-plugin
  * Set the source encoding to UTF-8

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 21 Jul 2015 14:52:02 +0200

shiro (1.2.3-1) unstable; urgency=low

  * Initial release (Closes: #726534)

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 10 Oct 2014 00:51:44 +0200
